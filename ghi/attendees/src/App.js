import './App.css';
import Nav from './Nav'


function App(props) {
  if (props.attendees === undefined) {
    return <div>No attendees found.</div>;
  }
  return (
    <>
    <Nav />
    <div className="container">
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Conference</th>
          </tr>
        </thead>
        <tbody>
        {props.attendees.map((attendee) => {
          return (
          <tr key={attendee.href}>
          <td>{ attendee.name }</td>
          <td>{ attendee.conference }</td>
          </tr>
          )
        })}
        </tbody>
      </table>
    </div>
    </>
  );

}
export default App;
