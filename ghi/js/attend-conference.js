window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingIcon = document.getElementById('loading-conference-spinner');
    // const attendeeForm  = document.getElementById('create-attendee-form')

    const url = 'http://localhost:8000/api/conferences'
    const response = await fetch(url);
    if(response.ok){
        const data = await response.json()
        for(let conference of data.conferences){
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name
            selectTag.appendChild(option)
        }
        loadingIcon.classList.add('d-none')
        selectTag.classList.remove('d-none')

        // attendeeForm.addEventListener('submit', formSubmitHandler);
    }
});


























































//   async function handleFormSubmit(event) {
//     event.preventDefault();

//     const form = event.target;
//     const formData = new FormData(form);

//     const formEntries = Array.from(formData.entries());
//     const options = {
//       method: 'POST',
//       body: JSON.stringify(Object.fromEntries(formEntries)),
//       headers: {
//         'Content-Type': 'application/json'
//       }
//     };

//     try {
//       const response = await fetch('http://localhost:8001/api/attendees/', options);
//       if (response.ok) {
//         console.log('Form submitted successfully');
//         // Handle successful form submission
//       } else {
//         console.log('Form submission failed');
//         // Handle form submission error
//       }
//     } catch (error) {
//       console.error('Error:', error);
//       // Handle network or other errors
//     }
//   }
