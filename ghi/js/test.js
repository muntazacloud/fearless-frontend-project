window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingIcon = document.getElementById('loading-conference-spinner');
    const attendeeForm  = document.getElementById('create-attendee-form')

    const url = 'http://localhost:8000/api/conferences'
    const response = await fetch(url);
    if(response.ok){
        const data = await response.json()
        for(let conference of data.conferences){
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name
            selectTag.appendChild(option)
        }
        loadingIcon.classList.add('d-none')
        selectTag.classList.remove('d-none')

        // attendeeForm.addEventListener('submit', formSubmitHandler);
    }

          // Submit event handler

attendeeForm.addEventListener('submit', async requestEvent => {

    requestEvent.preventDefault()

    // creating form data object from form
    let formData = new FormData(attendeeForm)
    // get new data from the form data entries
    const formObject = Object.fromEntries(formData.entries())

    // Creating option for the fetch, convert to Json with stringify and specifie header
    let options = {

        method: 'POST',
        body: JSON.stringify(formObject),
        header:{
            'Content-Type': 'application/json'
        }
    }
   // adding error Handler

   try{
       const attendeeURL = ("http://localhost:8001/api/attendees/")
       const attendeeResponse = await fetch(attendeeURL)
       if (attendeeResponse.ok){
         console.log("Congratulation form submitted succssfully")
       }else{
           console.log("Error Raised to submit form")
       }

   }catch(e){
       console.error("error catch", e)
   }

});

});


























































//   async function handleFormSubmit(event) {
//     event.preventDefault();

//     const form = event.target;
//     const formData = new FormData(form);

//     const formEntries = Array.from(formData.entries());
//     const options = {
//       method: 'POST',
//       body: JSON.stringify(Object.fromEntries(formEntries)),
//       headers: {
//         'Content-Type': 'application/json'
//       }
//     };

//     try {
//       const response = await fetch('http://localhost:8001/api/attendees/', options);
//       if (response.ok) {
//         console.log('Form submitted successfully');
//         // Handle successful form submission
//       } else {
//         console.log('Form submission failed');
//         // Handle form submission error
//       }
//     } catch (error) {
//       console.error('Error:', error);
//       // Handle network or other errors
//     }
//   }
