window.addEventListener("DOMContentLoaded", async () =>{

 const locationTag = document.getElementById('location');
 const formTag = document.getElementById('create-conference-form');


 const url = "http://localhost:8000/api/locations/"

 const response  = await fetch(url)

    if(response.ok){
        const data = await response.json()
        for(let location of data.locations){
        const options = document.createElement('option')

            options.value = location.id;
            options.innerHTML = location.name;
            locationTag.appendChild(options)

        }
    }

    // Now We Need To Make sure When User Press Create it would be saved to the data base
    // Use FormTag to Add Event Listener


    formTag.addEventListener("submit", async event =>{
        event.preventDefault();

         // create new FormData for the form, and  create  Entry... to turn to json
         const formData = new FormData(formTag);
         const formObject = Object.fromEntries(formData)
         const json = JSON.stringify(formObject)
         const  conferenceUrl  = "http://localhost:8000/api/conferences/"

        const fetchConfig = {
            method:'post',
            body: json,
            headers: {
                "Content-Type": 'application/json'
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig)

        if(response.ok){
           formTag.reset()
           const data = await response.json()
           console.log(data)

        }
    });

});
