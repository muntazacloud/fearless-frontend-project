
window.addEventListener("DOMContentLoaded", async () =>{
    const stateTag = document.getElementById('state')
    const formTag = document.getElementById('create-location-form')

    const url = "http://localhost:8000/api/states/"
    const response = await fetch(url)

    if(response.ok){
        const data = await  response.json();

        for(let state of data.states){

            const options = document.createElement("option");

            options.value = state.abbreviation
            options.innerHTML = state.name
            stateTag.appendChild(options)
        }
};

//  Adding Submit Event to the Button ,

formTag.addEventListener("submit", async event => {
    event.preventDefault();

    // Convert Data to Json in js => use JSON.stringify()

    const formData = new FormData(formTag);
    const entries = Object.fromEntries(formData)
    const convertToJson = JSON.stringify(entries)
    const locationUrl = "http://localhost:8000/api/locations/"

    const fetchConfig = {
        method: "post",
        body: convertToJson,
        headers: {
            'Content-Type': 'application/json'
        },
    };
    
    const locationResponse = await fetch(locationUrl, fetchConfig)

    if(locationResponse.ok){
            formTag.reset();
            const newLocation = await locationResponse.json()
            console.log(newLocation)

        }
});

});






// const locationResponse = await fetch(locationUrl, fetchConfig)


// "We need to add an event listener for when the DOM loads."
// "Let's declare a variable that will hold the URL for the API that we just created."
// "Let's fetch the URL. Don't forget the await keyword so that we get the response, not the Promise."
// "If the response is okay, then let's get the data using the .json method. Don't forget to await that, too."


// Get the select tag element by its id 'state'

// For each state in the states property of the data

  // Create an 'option' element

  // Set the '.value' property of the option element to the
  // state's abbreviation

  // Set the '.innerHTML' property of the option element to
  // the state's name

  // Append the option element as a child of the select tag
