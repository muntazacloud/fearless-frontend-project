window.addEventListener('DOMContentLoaded', async () => {
    function createCard(name, description, pictureUrl, startDate, endDate, location) {
      return `
       <div class="col">
        <div class="card shadow">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
          <small class="text-muted">${startDate} - ${endDate}</small>
        </div>
        </div>
        </div>
      `;
    }
    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        console.error('encountered an error');
      } else {
        const data = await response.json();
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details)
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const locations = details.conference.location.name;

            let startDate = new Date(details.conference.starts)
            let endDate = new Date(details.conference.ends)

           startDate = startDate.toLocaleDateString(); // this converts the date formate
           endDate = endDate.toLocaleDateString();

            const html = createCard(title, description, pictureUrl, startDate, endDate, locations);

            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }
      }
    } catch (e) {
      console.error('encountered an error');
    }
  });
