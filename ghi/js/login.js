window.addEventListener( 'DOMContentLoaded', () => {

    const form = document.getElementById('login-form');

    // Add Event Handler for the login form

    form.addEventListener( 'submit', async event =>{

        event.preventDefault();

        const dataEntries = Object.fromEntries(new FormData(form))

        const options = {
            method:'post',
            body: new FormData(form),
            // header: {
            //     'Content-Type': 'application/json',
            // }
            credentials: "include",
        };
        const url = 'http://localhost:8000/login/';
        const response = await fetch(url, options);

        if(response.ok){
            window.location.href = '/';
        }else{
            console.error(response);
        }

    } );
});
